# Work Managerment System（WMS）

💡 **「关于」**

  WMS办公管理系统是一款非常实用、方便、高效的管理系统，其针对企业考勤和办公用品进行管理的需求进行自动化开发

  `主要设计功能`
  1、部门信息管理  2、角色管理及切换  3、 雇员管理 4、请假管理 

  5、请假审批及进展  6、员工出勤管理 7、办公用品出入管理  8、办公用品使用申请 

  9、办公用品使用审核 10、权限分配

  此系统使用django框架进行搭建部署实现快速自动化开发，mysql作为数据存储管理工具，主要实现企业雇员的考勤记录和办公用品管理。

  对于角色切换主要分：[超级管理员]、[部门经理]、[雇员]等；

  出勤管理包括：[考勤打卡]、[查看个人信息]、[查看考勤信息]；

  请假管理分为：[提交请假申请]、[销假申请]、[请假审批进展];

  办公用品管理分为：[提交办公用品申请]、[查看申请记录]、[查看系统公告]等。

  部门经理可以和雇员同样操作，此外其还可以对雇员的请假申请、办公用品申请、考勤打卡等信息进行管理。
  超级管理员会不定时发送系统公告及进行全面管理和系统权限分配等，具有良好的实用价值和发展前景。

## 平台简介

💡 [work-managerment-system] WMadmain是一套全部开源的快速开发平台，毫无保留给个人及企业免费使用。

* 🧑‍🤝‍🧑前端采用
	[D2Admin](https://github.com/d2-projects/d2-admin) 
	[Vue](https://cn.vuejs.org/)
	[ElementUI](https://element.eleme.cn/)
	[nodejs](https://nodejs.org/en/).

* 👭后端采用 
    [Python] 一款面向对象的解释性语言工具 [Python3.7 Pycharm](https://www.python.org/)
	[Django] 框架以及强大的 [Django3.2 REST Framework](https://pypi.org/project/djangorestframework)
	[Mysql] 一款数据库管理高并发高可用的管理工具 [mysql8.0](https://www.mysql.com/).

* 👫权限认证使用
	[Django REST Framework SimpleJWT](https://pypi.org/project/djangorestframework-simplejwt)，支持多终端认证系统。

* 👬支持加载动态权限菜单，多方式轻松权限控制。
* 💏特别鸣谢：
	[D2Admin](https://github.com/d2-projects/d2-admin) 
	[Vue-Element-Admin](https://github.com/PanJiaChen/vue-element-admin)
	[jetbrains](https://www.jetbrains.com/) 为本开源项目提供免费的 IntelliJ IDEA 授权。


## 在线体验

👩‍👧‍👦账号密码

- 账号：superadmin 

- 密码：admin123456

## 内置功能

1.  👨‍⚕️菜单管理：配置系统菜单，操作权限，按钮权限标识、后端接口权限等。
2.  🧑‍⚕️部门管理：配置系统组织机构（公司、部门、角色）。
3.  👩‍⚕️角色管理：角色菜单权限分配、数据权限分配、设置角色按部门进行数据范围权限划分。
4.  🧑‍🎓权限管理：授权角色的权限范围。
5.  👨‍🎓员工管理：用户是系统操作者，该功能主要完成系统用户配置。
6.  👬离职名单：查询不同时间范围内不同部门离职人员名单或人员变动关系。
7.  🧑‍🔧物料管理：对系统中经常使用的一些较为固定的数据进行维护。
8.  🧑‍🔧地区管理：对省市县区域进行管理。
9.  📁附件管理：对平台上所有文件、图片等进行统一管理。
10.  🗓️操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。

## 准备工作
~~~
Python >= 3.8.0 (推荐3.8+版本)
nodejs >= 14.0 (推荐最新)
Mysql >= 5.7.0 (可选，默认数据库sqlite3，推荐8.0版本)
Redis(可选，最新版)
~~~

## 前端部署♝

```bash
# 克隆项目
git clone https://gitee.com/gshhshh/wms.git

# 进入项目目录
cd web

# 安装依赖
~~~~~bash
#先安装官网的nodejs然后设置好环境变量及相关信息
~~~~~
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev

# 浏览器访问 http://localhost:8080
# .env.development 文件中可配置启动端口等参数

# 构建生产环境
npm run build
```

## 后端开发💈

~~~bash
1. 进入项目目录 cd backend
2. 在项目根目录中，复制 ./conf/env.example.py 文件为一份新的到 ./conf 文件夹下，并重命名为 env.py
3. 在 env.py 中配置数据库信息
	mysql数据库版本建议：8.0
	mysql数据库字符集：utf8mb4
4. 安装依赖环境
	pip install -r requirements.txt
5. 执行迁移命令：
	python manage.py makemigrations
	python manage.py migrate
6. 初始化数据
	python manage.py init
7. 初始化省市县数据:
	python manage.py init_area
8. 启动项目
	python manage.py runserver 127.0.0.1:8000
或使用 daphne :
  daphne -b 0.0.0.0 -p 8000 application.asgi:application
~~~

### 访问项目

- 访问地址：[http://localhost:8080](http://localhost:8080) (默认为此地址，如有修改请按照配置文件)
- 账号：`superadmin` 密码：`admin123456`


### docker-compose 运行

~~~shell
# 先安装docker-compose (自行百度安装),执行此命令等待安装，如有使用celery插件请打开docker-compose.yml中celery 部分注释
docker-compose up -d
# 初始化后端数据(第一次执行即可)
docker exec -ti wms bash
python manage.py makemigrations 
python manage.py migrate
python manage.py init_area
python manage.py init
exit

前端地址：http://127.0.0.1:8080
后端地址：http://127.0.0.1:8080/api
# 在服务器上请把127.0.0.1 换成自己公网ip
账号：superadmin 密码：admin123456

# docker-compose 停止
docker-compose down
#  docker-compose 重启
docker-compose restart
#  docker-compose 启动时重新进行 build
docker-compose up -d --build
~~~